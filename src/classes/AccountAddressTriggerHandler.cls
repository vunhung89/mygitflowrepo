/**
* ClassName		:   AccountAddressTriggerHandler 
* Description	:   This is the handler class for AccountAddressTrigger
* @created		:   2016/07/06 vlhcnhung
* @modified		:   
*/
public with sharing class AccountAddressTriggerHandler {

	/**
	* onBeforeInsertUpdate
	* checks for a checkbox, and if the checkbox field is true, 
	* sets the Shipping Postal Code (whose API name is ShippingPostalCode) 
	* to be the same as the Billing Postal Code (BillingPostalCode)
	* @param accountList
	* @return　null
	* @created: 2016/07/06 vlhcnhung
	* @modified: 
	*/
	public void onBeforeInsertUpdate(List<Account> accountList){
		try{
			for(Account acc : accountList) {
				if(acc.Match_Billing_Address__c && !String.isBlank(acc.BillingPostalCode)) {
					acc.ShippingPostalCode = acc.BillingPostalCode;
				}
			}
		}catch(Exception ex){
			accountList.get(0).addError(ex.getMessage());
		}
	}
}
/**
* ClassName		:   AccountProcessor
* Description	:   
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
global class AccountProcessor{

	@future
	public static void countContacts(List<Id> accIdList) {
		List<Account> accList = new List<Account>();
		List<Contact> contList = new List<Contact>();

		// if a list of Account IDs is empty, end process
		if(accIdList.isEmpty()) {
			return;
		}

		// count the number of Contact records and update the 'Number_of_Contacts__c' field with this value
		for (AggregateResult ar : [SELECT AccountId, Count(Id) NumberOfContact FROM Contact WHERE AccountId IN : accIdList GROUP BY AccountId]){
			Account acc = new Account();
			acc.Id = (Id) ar.get('AccountId');
			acc.Number_of_Contacts__c = (Integer) ar.get('NumberOfContact');
			accList.add(acc);
		}

		if(!accList.isEmpty()){
			update accList;
		}		
	}
}
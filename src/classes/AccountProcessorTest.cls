/**
* ClassName     :   AccountProcessorTest
* Description   :   This apex class uses to cover code for the class "AccountProcessor"
* @created      :   2016/07/07 vlhcnhung
* @modified     :   
*/
@isTest
private class AccountProcessorTest {

	/**
	* testMethodCountContacts_1
	* a list of Account IDs is empty
	* @created	: 2016/07/07 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodCountContacts_1() {
		Account acc = new Account( Name = 'Test Name');
		insert acc;

		test.startTest();
			AccountProcessor.countContacts(new List<Id>());
		test.stopTest();

		acc = [SELECT Id, Name, Number_of_Contacts__c FROM Account WHERE Id = : acc.Id];
		system.assertEquals(null, acc.Number_of_Contacts__c);
	}

	/**
	* testMethodCountContacts_2
	* a list of Account IDs is not empty
	* @created	: 2016/07/07 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodCountContacts_2() {
		Account acc = new Account( Name = 'Test Name');
		insert acc;
		Contact cont = new Contact();
		cont.LastName = 'Test Name';
		cont.AccountId = acc.Id;
		insert cont;

		test.startTest();
			AccountProcessor.countContacts(new List<Id>{ acc.Id });
		test.stopTest();

		acc = [SELECT Id, Name, Number_of_Contacts__c FROM Account WHERE Id = : acc.Id];
		system.assertEquals(1, acc.Number_of_Contacts__c);
	}
}
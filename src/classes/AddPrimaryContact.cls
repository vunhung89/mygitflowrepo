/**
* ClassName		:   LeadProcessor 
* Description	:   a Queueable Apex class that inserts Contacts for Accounts
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
public class AddPrimaryContact implements Queueable {

	private Contact cont { get; set;}
	private String state { get; set;}

	/**
	* AddPrimaryContact
	* a constructor for the class that accepts as its first argument a Contact sObject 
	* and a second argument as a string for the State abbreviation
	* @param	: Contact
	* @param	: state
	* @return   : Lead records
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	public AddPrimaryContact(Contact con, String sta){
		this.cont = con;
		this.state = sta;
	}

	/**
	* execute
	* The execute method must query for a maximum of 200 Accounts with the BillingState 
	* specified by the State abbreviation passed into the constructor 
	* and insert the Contact sObject record associated to each Account
	* @param	: Contact
	* @param	: state
	* @return   : Lead records
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	public void execute(QueueableContext context) {
		List<Contact> contList = new List<Contact>();
		List<Account> accList = [SELECT Id, Name FROM Account WHERE BillingState = : state LIMIT 200];

		if(accList.isEmpty()) {
			return;
		}
		for(Account acc : accList){
            Contact con = this.cont.clone(false, false,false,false);
            con.AccountId = acc.Id;
            contList.add(con);
        }
		
		if(!contList.isEmpty()) {
			insert contList;
		}
	}

}
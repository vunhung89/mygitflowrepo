/**
* ClassName		:   AddPrimaryContactTest 
* Description	:   AddPrimaryContactTest
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
@isTest
public with sharing class AddPrimaryContactTest {

	/**
	* testMethodExecute_1
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	static testMethod void testMethodExecute_1() {
		List<Account> accList = new List<Account>();
		for(Integer i = 0; i < 50; i++){
			accList.add(new Account( Name = 'Test ' + i, BillingState = 'NY' ));
		}
		for(Integer i = 50; i < 100; i++){
			accList.add(new Account( Name = 'Test ' + i, BillingState = 'CA' ));
		}
		insert accList;
		Contact cont = new Contact();
		cont.LastName = 'Test Name';
		insert cont;

		test.startTest();
			AddPrimaryContact addPrimaryContact = new AddPrimaryContact(cont, 'HCM');
			system.enqueueJob(addPrimaryContact);
		test.stopTest();

		system.assertEquals(false, [SELECT Id FROM Contact].size() > 1);
	}

	/**
	* testMethodExecute_2
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	static testMethod void testMethodExecute_2() {
		List<Account> accList = new List<Account>();
		for(Integer i = 0; i < 50; i++){
			accList.add(new Account( Name = 'Test ' + i, BillingState = 'NY' ));
		}
		for(Integer i = 50; i < 100; i++){
			accList.add(new Account( Name = 'Test ' + i, BillingState = 'CA' ));
		}
		insert accList;
		Contact cont = new Contact();
		cont.LastName = 'Test Name';
		insert cont;

		test.startTest();
			AddPrimaryContact addPrimaryContact = new AddPrimaryContact(cont, 'CA');
			system.enqueueJob(addPrimaryContact);
		test.stopTest();

		system.assertEquals(51, [SELECT Id FROM Contact].size());
	}
}
/**
* ClassName		:   AnimalLocator
* Description	:   an Apex class that calls a REST endpoint
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
public class AnimalLocator {

	/**
	* getAnimalNameById
	* This method must call https://th-apex-http-callout.herokuapp.com/animals/:id, 
	* using the ID passed into the method. The method returns the value of the 'name' property
	* @param	: ID
	* @return   : name
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	public static String getAnimalNameById(Integer id) {
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals/' + id);
		request.setMethod('GET');
		HttpResponse response = http.send(request);
		String strResp = '';

		// If the request is successful, end process.
		if (response.getStatusCode() == 200) {
			// Deserializes the JSON string into collections of primitive data types.
			Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
			// Cast the values in the 'animals' key as a list
			Map<string, Object> animals = (map<string,object>) results.get('animal');
			strResp = String.valueof(animals.get('name'));
		}	

		return strResp;
	}
}
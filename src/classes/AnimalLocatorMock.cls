/**
* ClassName		:   AnimalLocatorMock
* Description	:   the callout response
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
@isTest
global class AnimalLocatorMock implements HttpCalloutMock {

	/**
	* respond
	* Create a fake response
	* @param	: HTTPRequest
	* @return   : HTTPResponse
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	global HTTPResponse respond(HTTPRequest request) {		
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		response.setBody('{"animal":{"id":1,"name":"chicken","eats":"chicken food","says":"cluck cluck"}}');
		response.setStatusCode(200);
		return response;
	}
}

/**
* ClassName     :   AnimalLocatorTest
* Description   :   This apex class uses to cover code for the class "AnimalLocator"
* @created      :   2016/07/07 vlhcnhung
* @modified     :   
*/
@isTest
private class AnimalLocatorTest {

	/**
	* testMethodGetAnimalNameById
	* @created	: 2016/07/07 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodGetAnimalNameById() {
		test.startTest();
			test.setMock(HttpCalloutMock.class, new AnimalLocatorMock());
			String strResp = AnimalLocator.getAnimalNameById(1);
		test.stopTest();

		system.assert(!String.isBlank(strResp));
	}
}
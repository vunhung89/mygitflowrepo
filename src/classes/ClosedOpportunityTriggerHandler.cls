/**
* ClassName		:   ClosedOpportunityTriggerHandler 
* Description	:   This is the handler class for ClosedOpportunityTrigger
* @created		:   2016/07/06 vlhcnhung
* @modified		:   
*/
public with sharing class ClosedOpportunityTriggerHandler {

	private static final String STAGENAME_CLOSED_WON = 'Closed Won';
	private static final String SUBJECT_FOLLOW_TASK = 'Follow Up Test Task';

	/**
	* onAfterInsertUpdate
	* if an opportunity is inserted or updated with a stage of 'Closed Won', 
	* it will have a task created with the subject 'Follow Up Test Task'
	* @param opportunityList
	* @return　null
	* @created: 2016/07/06 vlhcnhung
	* @modified: 
	*/
	public void onAfterInsertUpdate(List<Opportunity> opportunityList){
		try{
			List<Task> taskList = new List<Task>();

			for(Opportunity opp : opportunityList) {
				if(!String.isBlank(opp.StageName) && STAGENAME_CLOSED_WON.contains(opp.StageName)) {
					Task newTask = new Task();
					newTask.Subject = SUBJECT_FOLLOW_TASK;
					newTask.WhatId = opp.Id;
					taskList.add(newTask);
				}
			}

			if(!taskList.isEmpty()) {
				insert taskList;
			}
		}catch(Exception ex){
			opportunityList.get(0).addError(ex.getMessage());
		}
	}
}
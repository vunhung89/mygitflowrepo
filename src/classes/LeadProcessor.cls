/**
* ClassName		:   LeadProcessor 
* Description	:   Batch Apex to update Lead records
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
global class LeadProcessor implements Database.Batchable<sObject>, Database.Stateful {

	/**
	* start
	* collect all Lead records in the org
	* @param	: Database.BatchableContext BC
	* @return   : Lead records
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, LeadSource FROM Lead]);
	}

	/**
	* execute
	* update all Lead records in the org with the LeadSource value of 'Dreamforce'.
	* @param	: Database.BatchableContext BC
	* @param	: Lead records
	* @return   : null
	* @created  : 2016/07/07 vlhcnhung
	* @modified :
	*/
	global void execute(Database.BatchableContext BC, List<Lead> leadList) {
		for(Lead l : leadList) {
			l.LeadSource = 'Dreamforce';
		}
		if(!leadList.isEmpty()) {
			update leadList;
		}
	}

	/**
	* finish
	* 
	* @param	: Database.BatchableContext BC
	* @return   : null
	* @created  : 2015/09/18 Ksvc Nhung Vu
	* @created  : 2016/07/07 vlhcnhung
	* @modified :
	*/
	global void finish(Database.BatchableContext BC) {
		// do nothing
	}
}
/**
* ClassName		:   LeadProcessorTest 
* Description	:   LeadProcessorTest
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
@isTest
public with sharing class LeadProcessorTest {

	/**
	* testMethodExecute_1
	* List of Leads is empty
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	static testMethod void testMethodExecute_1() {
		test.startTest();
			LeadProcessor leadBatch = new LeadProcessor();
			Database.executebatch(leadBatch);
		test.stopTest();

		system.assert([SELECT Id, LeadSource FROM Lead WHERE LeadSource = 'Dreamforce'].isEmpty());
	}

	/**
	* testMethodExecute_2
	* List of Leads is empty
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	static testMethod void testMethodExecute_2() {
		List<Lead> leadList = new List<Lead>();
		for (Integer i = 0; i < 200; i++) {
			leadList.add(new Lead(
				LastName ='Test Name ' + i,
				Company = 'Test Comapny ' + i,
				Status = 'Open - Not Contacted'
			));
        }
        insert leadList;

		test.startTest();
			LeadProcessor leadBatch = new LeadProcessor();
			Database.executebatch(leadBatch);
		test.stopTest();

		system.assertEquals(200, [SELECT Id, LeadSource FROM Lead WHERE LeadSource = 'Dreamforce'].size());
	}
}
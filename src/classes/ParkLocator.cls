/**
* ClassName		:   ParkLocator
* Description	:   
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
public class ParkLocator {

	/**
	* country
	* uses the 'ParkService' class 
	* and returns an array of available park names for a particular country passed to the web service
	* @param	: ID
	* @return   : name
	* @created  : 2016/07/07 vlhcnhung
	* @modified : 
	*/
	public static String[] country(String country) {
		ParkService.ParksImplPort park = new ParkService.ParksImplPort();
		return park.byCountry(country);
	}
}
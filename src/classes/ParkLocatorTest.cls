/**
* ClassName     :   ParkLocatorTest
* Description   :   This apex class uses to cover code for the class "ParkLocator"
* @created      :   2016/07/07 vlhcnhung
* @modified     :   
*/
@isTest
private class ParkLocatorTest {

	/**
	* testMethodCountry
	* @created	: 2016/07/07 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodCountry() {
		test.startTest();
			test.setMock(WebServiceMock.class, new ParkServiceMock());
			String[] parks = ParkLocator.country('Japan');
		test.stopTest();

		system.assertEquals(new List<String>{'Park 1', 'Park 2'}, parks);
	}
}
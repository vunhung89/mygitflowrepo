/**
* ClassName		:   ParkServiceMock
* Description	:   
* @created		:   2016/07/07 vlhcnhung
* @modified		:   
*/
@isTest
global class ParkServiceMock implements WebServiceMock {

	global void doInvoke(Object stub, Object request, Map<String, Object> response,
						String endpoint, String soapAction, String requestName,
						String responseNS, String responseName, String responseType) {
		// start - specify the response you want to send
		ParkService.byCountryResponse response_x = new ParkService.byCountryResponse();
		response_x.return_x = new String[]{'Park 1', 'Park 2'};
		response.put('response_x', response_x);
   }
}
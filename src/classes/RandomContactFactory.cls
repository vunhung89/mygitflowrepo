/**
* ClassName		:   RandomContactFactory 
* Description	:   RandomContactFactory
* @created		:   2016/07/06 vlhcnhung
* @modified		:   
*/
public class RandomContactFactory {

	/**
	* generateRandomContacts
	* Create an Apex class that returns a list of contacts based on two incoming parameters: 
	* one for the number of contacts to generate, and the other for the last name
	* @param: numberContact
	* @param: lastName
	* @return:　contactList
	* @created: 2016/07/06 vlhcnhung
	* @modified: 
	*/
	public static List<Contact> generateRandomContacts(Integer numberContacts, String lastName) {
		List<Contact> contactList = new List<Contact>();

		for(Integer i = 0; i < numberContacts; i++) {
			Contact newContact = new Contact();
			newContact.LastName = lastName;
			newContact.FirstName = 'Test ' + String.valueOf(i + 1);

			contactList.add(newContact);
		}

		return contactList;
	}
}
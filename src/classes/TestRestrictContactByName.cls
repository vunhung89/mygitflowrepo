/**
* ClassName     :   TestRestrictContactByName
* Description   :   This apex class uses to cover code for the trigger "RestrictContactByName"
* @created      :   2016/07/06 vlhcnhung
* @modified     :   
*/
@isTest
private class TestRestrictContactByName {

	/**
	* testMethodBeforeInsert
	* 
	* @created	: 2016/07/06 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodBeforeInsert() {
		Contact newContact = new Contact();
		newContact.LastName = 'INVALIDNAME';

		test.startTest();
			try {
				insert newContact;
			} catch (DmlException dmlEx) {
				Boolean msgFound = false;
				String msgError = 'The Last Name "' + newContact.LastName + '" is not allowed for DML';
				for(Apexpages.Message msg : ApexPages.getMessages()) {
					if (msg.getDetail() == msgError) msgFound = true;
				}
				system.assert(msgFound);
			}
		test.stopTest();
	}

	/**
	* testMethodBeforeUpdate
	* 
	* @created	: 2016/07/06 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodBeforeUpdate() {
		Contact newContact = new Contact();
		newContact.LastName = 'Test Name';
		insert newContact;

		test.startTest();
			try {
				newContact.LastName = 'INVALIDNAME';
				update newContact;
			} catch (DmlException dmlEx) {
				Boolean msgFound = false;
				String msgError = 'The Last Name "' + newContact.LastName + '" is not allowed for DML';
				for(Apexpages.Message msg : ApexPages.getMessages()) {
					if (msg.getDetail() == msgError) msgFound = true;
				}
				system.assert(msgFound);
			}
		test.stopTest();
	}
}
/**
* ClassName		:   TestVerifyDate 
* Description	:   This apex class uses to cover code for the class "VerifyDate"
* @created		:   2016/07/06 vlhcnhung
* @modified		:   
*/
@isTest
private class TestVerifyDate {

	/**
	* testMethodCheckDates_1
	* if date2 is within the next 30 days of date1, use date2
	* @created	: 2016/07/06 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodCheckDates_1() {
		Date date1 = Date.valueOf('2016-07-01');
		Date date2 = Date.valueOf('2016-07-15');

		test.startTest();
			Date dateCompare = VerifyDate.CheckDates(date1, date2);
		test.stopTest();

		system.assertEquals(date2, dateCompare);
	}

	/**
	* testMethodCheckDates_2
	* if date2 is not within the next 30 days of date1, use the end of the month (date1)
	* @created	: 2016/07/06 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodCheckDates_2() {
		Date date1 = Date.valueOf('2016-07-01');
		Date date2 = Date.valueOf('2016-08-15');

		test.startTest();
			Date dateCompare = VerifyDate.CheckDates(date1, date2);
		test.stopTest();

		system.assertEquals(Date.valueOf('2016-07-31'), dateCompare);
	}

	/**
	* testMethodCheckDates_3
	* if date2 is not within the next 30 days of date1, use the end of the month (date1)
	* @created	: 2016/07/06 vlhcnhung
	* @modified	: 
	*/
	static testMethod void testMethodCheckDates_3() {
		Date date1 = Date.valueOf('2016-07-01');
		Date date2 = Date.valueOf('2016-06-15');

		test.startTest();
			Date dateCompare = VerifyDate.CheckDates(date1, date2);
		test.stopTest();

		system.assertEquals(Date.valueOf('2016-07-31'), dateCompare);
	}
}
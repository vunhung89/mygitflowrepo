/**
* ClassName		:   AccountAddressTrigger 
* Description	:   AccountAddressTrigger
* @created  	:   2016/07/06 vlhcnhung
* @modified 	:   
*/
trigger AccountAddressTrigger on Account (before insert, before update) {
	
	if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
		AccountAddressTriggerHandler handler = new AccountAddressTriggerHandler();
		handler.onBeforeInsertUpdate(trigger.new);
	}
}
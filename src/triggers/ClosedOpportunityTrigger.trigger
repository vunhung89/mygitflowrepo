/**
* ClassName		:   ClosedOpportunityTrigger 
* Description	:   Create an Apex trigger for Opportunity that adds a task to any opportunity set to 'Closed Won'.
* @created  	:   2016/07/06 vlhcnhung
* @modified 	:   
*/
trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {

	if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
		ClosedOpportunityTriggerHandler handler = new ClosedOpportunityTriggerHandler();
		handler.onAfterInsertUpdate(trigger.new);
	}
}